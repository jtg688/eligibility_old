from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import Job
import eligibility.date_helper as dh


class DetermineSemester(TestCase):
    def test_spring(self):
        spring_day = dt.strptime('20190310', '%Y%m%d')
        result = dh.determine_semester(spring_day)
        self.assertEqual(result, 2)

    def test_summer(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_semester(summer_day)
        self.assertEqual(result, 6)

    def test_fall_1(self):
        fall_day = dt.strptime('20200110', '%Y%m%d')
        result = dh.determine_semester(fall_day)
        self.assertEqual(result, 9)

    def test_fall_2(self):
        fall_day = dt.strptime('20191001', '%Y%m%d')
        result = dh.determine_semester(fall_day)
        self.assertEqual(result, 9)


class DetermineSummerSemester(TestCase):
    def test_summer_1(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_summer_semester(summer_day, '20196')
        self.assertEqual(result, '6-1')

    def test_summer_2(self):
        summer_day = dt.strptime('20190720', '%Y%m%d')
        result = dh.determine_summer_semester(summer_day, '20196')
        self.assertEqual(result, '6-2')

class DetermineCCYS(TestCase):
    def test_spring(self):
        spring_day = dt.strptime('20190310', '%Y%m%d')
        result = dh.determine_ccys(spring_day)
        self.assertEqual(result, '20192')

    def test_summer(self):
        summer_day = dt.strptime('20190710', '%Y%m%d')
        result = dh.determine_ccys(summer_day)
        self.assertEqual(result, '20196')

    def test_fall_1(self):
        fall_day = dt.strptime('20200110', '%Y%m%d')
        result = dh.determine_ccys(fall_day)
        self.assertEqual(result, '20199')

    def test_fall_2(self):
        fall_day = dt.strptime('20191001', '%Y%m%d')
        result = dh.determine_ccys(fall_day)
        self.assertEqual(result, '20189')


class JobsOverlap(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.start_date = dt.strptime('20190720', '%Y%m%d')
        self.job.end_date = dt.strptime('20190730', '%Y%m%d')
        self.addl_job = {
            'start_date': dt.strptime('20190720', '%Y%m%d'),
            'end_date': dt.strptime('20190730', '%Y%m%d'),
            }

    def test_overlap_1(self):
        """jobs are completely alligned"""
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_2(self):
        """job starts before addl_job"""
        self.job.start_date = dt.strptime('20190710', '%Y%m%d')
        self.addl_job['end_date'] = dt.strptime('20190830', '%Y%m%d'),
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_3(self):
        """job starts after addl_job"""
        self.job.start_date = dt.strptime('20190725', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_4(self):
        """job completely overlaps addl_job"""
        self.job.start_date = dt.strptime('20190501', '%Y%m%d')
        self.job.end_date = dt.strptime('20191001', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_overlap_5(self):
        """job completely within addl_job"""
        self.job.start_date = dt.strptime('20190721', '%Y%m%d')
        self.job.end_date = dt.strptime('20190725', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertTrue(result)

    def test_no_overlap(self):
        self.job.start_date = dt.strptime('20190201', '%Y%m%d')
        self.job.end_date = dt.strptime('20190401', '%Y%m%d')
        result = dh.jobs_overlap(self.addl_job, self.job)
        self.assertFalse(result)

