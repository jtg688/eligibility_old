from datetime import datetime as dt
from django.core.exceptions import ImproperlyConfigured

try:
    from django.conf import settings
    BATCH = settings.BATCH
except ImproperlyConfigured:
    BATCH = True

from .constants import WS_FULL_TIME
from .rules import *


class JobValidator(object):
    def __init__(self, job, student):
        self.job = job
        self.student = student
        self.criticality = None
        self.messages = []
        self.statuses = []
        self.eligibility = None

    def validate_assignment(self):
        self.run_rules()
        self.set_criticality()
        self.set_eligibility()

    def run_rules(self):
        #run_rules must be extended in each subclass as needed

        try:
            msg, status = old_enough(self.student.age)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msg, status = correct_status(self.student, self.job)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msg, status = student_is_enrolled(self.student)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        if self.job.is_retro_start and not BATCH:
            msg = ('This appointment is in the past; we cannot verify the '
                'student was eligible for this job at the time of hire.')
            self.messages.append(msg)
            self.statuses.append('non-crit')

    def set_criticality(self):
        if 'crit' in self.statuses:
            self.criticality = 'Critical'
        elif 'non-crit' in self.statuses:
            self.criticality = 'Warning'
        else:
            self.criticality = None

    def set_eligibility(self):
        if self.criticality:
            self.eligibility = 0
        else:
            self.eligibility = 1


class UGNAValidator(JobValidator):
    def run_rules(self):
        super(UGNAValidator, self).run_rules()


        try:
            if self.job.start_date >= self.student.graduation_date:
                post_grad = True
            else:
                post_grad = False
        except TypeError:
            post_grad = False

        if post_grad:
            try:
                msg, status = post_graduation(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        else:
            try:
                msg, status = credit_hours_met(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        if self.job.ccyys_to_check[-1] != '6':
            try:
                msg, status = max_scheduled_hours_per_citizenship(
                    self.student,
                    self.job,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        try:
            msgs, statuses = total_scheduled_work_hours(self.job, self.student)
            self.messages += msgs
            self.statuses += statuses
        except TypeError:
            pass

        #IMPORTANT: This rule must fire last!!!
        if (BATCH and self.messages != []) or not BATCH:
            try:
                msg, status = quantity_of_work(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass


class UGANTValidator(JobValidator):
    def run_rules(self):
        super(UGANTValidator, self).run_rules()

        try:
            if self.job.start_date >= self.student.graduation_date:
                post_grad = True
            else:
                post_grad = False
        except TypeError:
            post_grad = False

        if post_grad:
            try:
                msg, status = post_graduation(
                    self.student,
                    self.job,
                    min_enrollment=12,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        else:
            try:
                msg, status = credit_hours_met(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        try:
            msg, status = gpa_met(self.student.gpa)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            if self.job.ccyys_to_check[-1] != '6':
                msg, status = max_scheduled_work_hours(self.job)
            else:
                msg, status = max_scheduled_work_hours(self.job, 40)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msgs, statuses = total_scheduled_work_hours(self.job, self.student)
            self.messages += msgs
            self.statuses += statuses
        except TypeError:
            pass

        try:
            msg, status = is_degree_seeking(self.student)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        #IMPORTANT: This rule must fire last!!!
        if (BATCH and self.messages != []) or not BATCH:
            try:
                msg, status = quantity_of_work(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass


class WSValidator(JobValidator):
    def run_rules(self):
        super(WSValidator, self).run_rules()

        try:
            msg, status = is_degree_seeking(self.student)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msg, status = sap_status(self.student)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msg, status = ws_check(self.student)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msg, status = max_scheduled_work_hours(self.job)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            msgs, statuses = total_scheduled_work_hours(self.job, self.student)
            self.messages += msgs
            self.statuses += statuses
        except TypeError:
            pass

        try:
            if self.job.time_type == WS_FULL_TIME:
                msg, status = ws_ft_dates_valid(self.job)
                self.messages.append(msg)
                self.statuses.append(status)
        except TypeError:
            pass


class WSAcaValidator(WSValidator):
    def run_rules(self):
        super(WSAcaValidator, self).run_rules()

        try:
            if self.student.is_undergrad():
                req_gpa = 2
            else:
                req_gpa = 3
            msg, status = gpa_met(self.student.gpa, required_gpa=req_gpa)
            self.messages.append(msg)
            self.statuses.append(status)
        except TypeError:
            pass

        try:
            if self.job.start_date >= self.student.graduation_date:
                post_grad = True
            else:
                post_grad = False
        except TypeError:
            post_grad = False

        if post_grad:
            try:
                msg, status = post_graduation(
                    self.student,
                    self.job,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        else:
            try:
                if self.student.is_undergrad():
                    if self.job.time_type == WS_FULL_TIME:
                        long_sem_hours = 0
                        spring_hours = 12
                        summer_hours = 3
                    else:
                        long_sem_hours = 12
                        spring_hours = 0
                        summer_hours = 6
                else:
                    if self.job.time_type == WS_FULL_TIME:
                        long_sem_hours = 0
                        spring_hours = 0
                        summer_hours = 3
                    else:
                        long_sem_hours = 9
                        spring_hours = 0
                        summer_hours = 5

                msg, status = ws_credit_hours_met(
                    self.student,
                    self.job,
                    long_sem_hours=long_sem_hours,
                    spring_hours=spring_hours,
                    summer_hours=summer_hours,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        #IMPORTANT: This rule must fire last!!!
        if (BATCH and self.messages != []) or not BATCH:
            try:
                msg, status = quantity_of_work(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass


class WSNonAcaValidator(WSValidator):
    def run_rules(self):
        super(WSNonAcaValidator, self).run_rules()

        try:
            if self.job.start_date >= self.student.graduation_date:
                post_grad = True
            else:
                post_grad = False
        except TypeError:
            post_grad = False


        if post_grad:
            try:
                msg, status = post_graduation(
                    self.student,
                    self.job,
                    allow_grad_students=True,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

        else:
            try:
                if self.job.time_type == WS_FULL_TIME:
                    long_sem_hours = 0
                    spring_hours = 0
                    summer_hours = 0
                elif self.student.is_undergrad():
                    long_sem_hours = 6
                    spring_hours = 0
                    summer_hours = 6
                else:
                    long_sem_hours = 6
                    spring_hours = 0
                    summer_hours = 5

                msg, status = ws_credit_hours_met(
                    self.student,
                    self.job,
                    long_sem_hours=long_sem_hours,
                    spring_hours=spring_hours,
                    summer_hours=summer_hours,
                    )
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass


        #IMPORTANT: This rule must fire last!!!
        if (BATCH and self.messages != []) or not BATCH:
            try:
                msg, status = quantity_of_work(self.student, self.job)
                self.messages.append(msg)
                self.statuses.append(status)
            except TypeError:
                pass

