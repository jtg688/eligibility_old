import logging

from django.conf import settings

from .constants import (
    JOB_FAMILIES,
    WS_STATE_EX_CODE_ACA,
    WS_STATE_EX_CODE_NON_ACA,
    )
from .validators import (
    UGNAValidator,
    UGANTValidator,
    WSAcaValidator,
    WSNonAcaValidator,
    )

import lxml.objectify

from soaptools.soap_request import SoapRequest, SoapRequestError


class MFError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


class InvalidJobProfileError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


class InvalidStudentError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)


def determine_eligibility(job, student):
    eligibility = {
        'eligible': 0,
        'warning_level': None,
        'msg': None
    }
    logging.debug('Checking eligibility for {0} for job {1}.'.format(
        student.student_id, job.job_profile,
        ))

    if student.is_on_exception_table():
        eligibility['eligible'] = 1
        eligibility['warning_level'] = None
        eligibility['msg'] = 'Student is on exception table.'

    elif job.job_profile[0] == 'G':
        eligibility['eligible'] = student.grad_eligibility
        eligibility['warning_level'] = student.grad_criticality
        eligibility['msg'] = student.get_grad_messages()

    elif job.job_family == JOB_FAMILIES['undergrad_non_aca']:
        validator = UGNAValidator(job, student)
        validator.validate_assignment()

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.criticality
        eligibility['msg'] = ' '.join(validator.messages)

    elif job.job_family == JOB_FAMILIES['undergrad_aca_nt']:
        validator = UGANTValidator(job, student)
        validator.validate_assignment()

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.criticality
        eligibility['msg'] = ' '.join(validator.messages)

    elif job.job_family == JOB_FAMILIES['ws']:
        if job.job_classification == WS_STATE_EX_CODE_ACA:
            validator = WSAcaValidator(job, student)
            validator.validate_assignment()

        elif job.job_classification == WS_STATE_EX_CODE_NON_ACA:
            validator = WSNonAcaValidator(job, student)
            validator.validate_assignment()

        else:
            logging.error('Invalid work study job classification.')
            raise InvalidJobProfileError(
                '{0} is not a valid work study job classification.'.format(
                job.job_classification
                ))

        eligibility['eligible'] = validator.eligibility
        eligibility['warning_level'] = validator.criticality
        eligibility['msg'] = ' '.join(validator.messages)

    else:
        logging.error('Invalid job family.')
        raise InvalidJobProfileError('{0} is not a valid job family.'.format(
            job.job_family
            ))

    logging.debug(eligibility)

    return eligibility


def respond_to_workday(eligibility, event):
    eligible = eligibility['eligible']
    level = eligibility['warning_level']

    ineligible_node = """
        <bsvc:Student_Employment_Eligibility_Reason_Data>
           <bsvc:Reason_for_Ineligibility>{0}</bsvc:Reason_for_Ineligibility>
           <bsvc:Critical>{1}</bsvc:Critical>
        </bsvc:Student_Employment_Eligibility_Reason_Data>
    """.format(
        eligibility['msg'],
        1 if level == 'Critical' else 0,
        )

    my_xml = """
      <bsvc:Put_Student_Employment_Eligibility_Status_Request bsvc:version="{0}">
         <bsvc:Student_Employment_Eligibility_Event_Reference bsvc:Descriptor="?">
            <bsvc:ID bsvc:type="Student_Employment_Eligibility_Event_ID">{1}</bsvc:ID>
         </bsvc:Student_Employment_Eligibility_Event_Reference>
         <bsvc:Student_Employment_Eligibility_Event_Data>
            <bsvc:Eligible>{2}</bsvc:Eligible>
            {3}
         </bsvc:Student_Employment_Eligibility_Event_Data>
      </bsvc:Put_Student_Employment_Eligibility_Status_Request>
    """.format(
        settings.WD_API_VERSION,
        event,
        eligible if eligible is 1 else 0,
        ineligible_node if eligible is 0 else '',
        )

    ns = {'bsvc': 'urn:com.workday/bsvc'}
    url = '{0}Staffing/{1}'.format(
        settings.WD_API_URL,
        settings.WD_API_VERSION,
        )
    username = settings.WD_USER
    password = settings.WD_PASS

    sreq = SoapRequest(url, username, password, nsmap=ns)
    sreq.append_to_body(my_xml)

    logging.debug(my_xml)

    response = sreq.send_request()
    resp = lxml.etree.fromstring(response.text.encode('utf-8'))

    try:
        fault = resp.find('.//faultstring').text
        logging.error(fault)
    except AttributeError:
        pass
