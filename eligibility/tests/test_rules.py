from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import Student, Job
import eligibility.rules as rules


class TestOldEnough(TestCase):
    def test_old_enough(self):
        self.assertIsNone(rules.old_enough(18))

    def test_not_old_enough(self):
        self.assertIsInstance(rules.old_enough(14), tuple)
        self.assertIsInstance(rules.old_enough(14)[0], str)
        self.assertEqual(rules.old_enough(14)[1], 'crit')


class TestCorrectStatus(TestCase):
    def test_undergrad_with_grad_job(self):
        student = Student()
        student.grad_status = 'undergrad'
        job = Job()
        job.job_profile = 'G'
        self.assertIsInstance(rules.correct_status(student, job), tuple)
        msg = 'Job requires {0}, student is {1}.'.format(
            'grad',
            student.grad_status,
            )
        self.assertEqual(rules.correct_status(student, job)[0], msg)
        self.assertEqual(rules.correct_status(student, job)[1], 'crit')

    def test_grad_with_undergrad_job(self):
        student = Student()
        student.grad_status = 'grad'
        job = Job()
        job.job_profile = 'U'
        self.assertIsInstance(rules.correct_status(student, job), tuple)
        msg = 'Job requires {0}, student is {1}.'.format(
            'undergrad',
            student.grad_status,
            )
        self.assertEqual(rules.correct_status(student, job)[0], msg)
        self.assertEqual(rules.correct_status(student, job)[1], 'crit')

    def test_undergrad_with_undergrad_job(self):
        student = Student()
        student.grad_status = 'undergrad'
        job = Job()
        job.job_profile = 'U'
        self.assertIsNone(rules.correct_status(student, job))

    def test_grad_with_grad_job(self):
        student = Student()
        student.grad_status = 'grad'
        job = Job()
        job.job_profile = 'G'
        self.assertIsNone(rules.correct_status(student, job))

    def test_undergrad_with_ws_job(self):
        student = Student()
        student.grad_status = 'undergrad'
        job = Job()
        job.job_profile = 'W'
        self.assertIsNone(rules.correct_status(student, job))

    def test_grad_with_ws_job(self):
        student = Student()
        student.grad_status = 'grad'
        job = Job()
        job.job_profile = 'W'
        self.assertIsNone(rules.correct_status(student, job))

    def test_invalid_job_profile(self):
        student = Student()
        student.grad_status = 'undergrad'
        job = Job()
        job.job_profile = 'X'
        self.assertRaises(Exception, rules.correct_status, [student, job])


class TestStudentIsEnrolled(TestCase):
    def test_student_is_enrolled(self):
        student = Student()
        student.current_sem_status = '2'
        self.assertIsNone(rules.student_is_enrolled(student))

    def test_student_is_not_enrolled(self):
        student = Student()
        student.current_sem_status = '8'
        self.assertIsInstance(rules.student_is_enrolled(student), tuple)
        self.assertIsInstance(rules.student_is_enrolled(student)[0], str)
        self.assertEqual(rules.student_is_enrolled(student)[1], 'crit')
        student.current_sem_status = '9'
        self.assertIsInstance(rules.student_is_enrolled(student), tuple)
        self.assertIsInstance(rules.student_is_enrolled(student)[0], str)
        self.assertEqual(rules.student_is_enrolled(student)[1], 'crit')

    def test_enrollment_exception(self):
        student = Student()
        student.current_sem_status = '1'
        self.assertRaises(Exception, rules.student_is_enrolled, student)


class TestGPAMet(TestCase):
    def test_gpa_is_not_met(self):
        self.assertIsInstance(rules.gpa_met(1.5), tuple)
        self.assertIsInstance(rules.gpa_met(1.5)[0], str)
        self.assertEqual(rules.gpa_met(1.5)[1], 'crit')

    def test_gpa_is_met(self):
        self.assertIsNone(rules.gpa_met(3.0))

    def test_gpa_is_zero(self):
        self.assertIsNone(rules.gpa_met(0))


class TestCreditHoursMet(TestCase):
    def setUp(self):
        self.real_summer = rules.summer_credit_hours_met
        self.real_long_sem = rules.long_sem_credit_hours_met
        rules.summer_credit_hours_met = mock.Mock(return_value='summer')
        rules.long_sem_credit_hours_met = mock.Mock(return_value='long_sem')
        self.job = Job()
        self.student = Student()

    def tearDown(self):
        rules.summer_credit_hours_met = self.real_summer
        rules.long_sem_credit_hours_met = self.real_long_sem

    def test_summer(self):
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        self.assertEqual(
            rules.credit_hours_met(self.student, self.job), 'summer'
            )

    def test_long_sem(self):
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')
        self.assertEqual(
            rules.credit_hours_met(self.student, self.job), 'long_sem'
            )


class TestLongSemCreditHoursMet(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.job_profile = 'U0074'
        self.student = Student()
        self.student.hr_deadline = dt.now() + timedelta(days=1)
        self.student.current_sem_hours = 12

    def test_success_default_hours(self):
        self.assertIsNone(
            rules.long_sem_credit_hours_met(self.student, self.job)
            )

    def test_success_custom_hours(self):
        self.assertIsNone(
            rules.long_sem_credit_hours_met(
                self.student,
                self.job,
                long_sem_hours=12,
                )
            )

    def test_fail_crit(self):
        long_sem_hours = 12
        self.student.current_sem_hours = 3
        self.student.hr_deadline = dt.now() - timedelta(days=1)
        result = rules.long_sem_credit_hours_met(
                self.student,
                self.job,
                long_sem_hours=long_sem_hours,
                )
        self.assertIsInstance(result, tuple)
        msg = '{0} credit hours required, but student only has {1}.'.format(
                long_sem_hours, self.student.current_sem_hours
            )
        self.assertEqual(result[0],msg)
        self.assertEqual(result[1], 'crit')

    def test_fail_noncrit(self):
        long_sem_hours = 12
        self.student.current_sem_hours = 3
        result = rules.long_sem_credit_hours_met(
                self.student,
                self.job,
                long_sem_hours=long_sem_hours,
                )
        self.assertIsInstance(result, tuple)
        msg = '{0} credit hours required, but student only has {1}.'.format(
                long_sem_hours, self.student.current_sem_hours
            )
        self.assertEqual(result[0],msg)
        self.assertEqual(result[1], 'non-crit')


class TestSummerCreditHoursMet(TestCase):
    def setUp(self):
        self.original_current_sem = rules.CURRENT_SEM
        self.job = Job()
        self.job.job_profile = 'U0074'
        self.student = Student()
        self.student.hr_deadline = dt.now() + timedelta(days=1)
        self.student.current_sem_hours = 12
        self.student.prev_sem_hours = 12
        self.job.job_family = 'Undergraduate Student Academic'

    def tearDown(self):
        rules.CURRENT_SEM = self.original_current_sem

    def test_success_aca(self):
        self.assertIsNone(
            rules.summer_credit_hours_met(self.student, self.job)
            )

    def test_success_non_aca(self):
        self.job.job_family = 'Undergraduate Student Non-Academic'
        self.assertIsNone(
            rules.summer_credit_hours_met(self.student, self.job)
            )

    def test_fail_in_spring_noncrit(self):
        rules.CURRENT_SEM = 2
        self.student.current_sem_hours = 3
        self.student.prev_sem_hours = 3
        result = rules.summer_credit_hours_met(
                self.student,
                self.job,
                )
        self.assertIsInstance(result, tuple)
        self.assertIn('is not enrolled', result[0])
        self.assertEqual(result[1], 'non-crit')

    def test_fail_in_summer_noncrit(self):
        rules.CURRENT_SEM = 6
        self.student.current_sem_hours = 3
        self.student.prev_sem_hours = 3
        result = rules.summer_credit_hours_met(
                self.student,
                self.job,
                )
        self.assertIsInstance(result, tuple)
        self.assertIn('was not enrolled', result[0])
        self.assertEqual(result[1], 'non-crit')

    def test_fail_in_summer_crit(self):
        rules.CURRENT_SEM = 6
        self.student.current_sem_hours = 3
        self.student.prev_sem_hours = 3
        self.student.hr_deadline = dt.now() - timedelta(days=1)
        result = rules.summer_credit_hours_met(
                self.student,
                self.job,
                )
        self.assertIsInstance(result, tuple)
        self.assertIn('was not enrolled', result[0])
        self.assertEqual(result[1], 'crit')


class TestWSCreditHoursMet(TestCase):
    def setUp(self):
        self.real_ws_summer = rules.ws_summer_credit_hours_met
        self.real_long_sem = rules.long_sem_credit_hours_met
        rules.ws_summer_credit_hours_met = mock.Mock(return_value='summer')
        rules.long_sem_credit_hours_met = mock.Mock(return_value='long_sem')
        self.job = Job()
        self.student = Student()

    def tearDown(self):
        rules.ws_summer_credit_hours_met = self.real_ws_summer
        rules.long_sem_credit_hours_met = self.real_long_sem

    def test_summer(self):
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        self.assertEqual(
            rules.ws_credit_hours_met(self.student, self.job), 'summer'
            )

    def test_long_sem(self):
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')
        self.assertEqual(
            rules.ws_credit_hours_met(self.student, self.job), 'long_sem'
            )


class TestWSSummerCreditHoursMet(TestCase):
    def setUp(self):
        self.original_current_sem = rules.CURRENT_SEM
        self.student = Student()
        self.student.hr_deadline = dt.now() + timedelta(days=1)
        self.student.current_sem_hours = 12
        self.student.prev_sem_hours = 12
        self.job = Job()
        self.job.job_family = 'Undergraduate Student Academic'
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')

    def tearDown(self):
        rules.CURRENT_SEM = self.original_current_sem

    def test_from_spring_with_spring_hours(self):
        rules.CURRENT_SEM = 6
        result = rules.ws_summer_credit_hours_met(
            self.student,
            self.job,
            spring_hours=6,
            )
        self.assertIsNone(result)

    def test_from_summer_with_spring_hours(self):
        rules.CURRENT_SEM = 6
        result = rules.ws_summer_credit_hours_met(
            self.student,
            self.job,
            spring_hours=6,
            )
        self.assertIsNone(result)

    def test_no_spring_hours_pass(self):
        result = rules.ws_summer_credit_hours_met(
            self.student,
            self.job,
            )
        self.assertIsNone(result)

    def test_no_spring_hours_fail(self):
        summer_hours = 20
        result = rules.ws_summer_credit_hours_met(
            self.student,
            self.job,
            summer_hours=summer_hours,
            )
        msg = '{0} credit hours required, but student only has {1}.'.format(
            summer_hours,
            self.student.current_sem_hours
            )
        self.assertEqual(result[0], msg)
        self.assertEqual(result[1], 'non-crit')

    def test_no_spring_hours_fail_crit(self):
        self.student.hr_deadline = dt.now() - timedelta(days=1)
        summer_hours = 20
        result = rules.ws_summer_credit_hours_met(
            self.student,
            self.job,
            summer_hours=summer_hours,
            )
        msg = '{0} credit hours required, but student only has {1}.'.format(
            summer_hours,
            self.student.current_sem_hours
            )
        self.assertEqual(result[0], msg)
        self.assertEqual(result[1], 'crit')


class TestMaxScheduledHoursPerCitizenship(TestCase):
    def setUp(self):
        self.real_max_hours = rules.max_scheduled_work_hours
        self.student = Student()
        self.job = Job()
        self.job.scheduled_hours = 30

    def tearDown(self):
        rules.max_scheduled_work_hours = self.real_max_hours

    def test_pass_is_citizen(self):
        self.student.is_us_citizen = mock.Mock(return_value=True)
        self.assertIsNone(
            rules.max_scheduled_hours_per_citizenship(self.student, self.job)
            )

    def test_fail_is_citizen(self):
        self.job.scheduled_hours = 50
        self.student.is_us_citizen = mock.Mock(return_value=True)
        result = rules.max_scheduled_hours_per_citizenship(
            self.student,
            self.job,
            )
        msg = 'Student cannot be assigned over 40 hours for this job.'
        self.assertEqual(result[0], msg)
        self.assertEqual(result[1], 'crit')

    def test_fail_is_not_citizen(self):
        self.student.is_us_citizen = mock.Mock(return_value=False)
        result = rules.max_scheduled_hours_per_citizenship(
            self.student,
            self.job,
            )
        msg = 'Student cannot be assigned over 20 hours for this job.'
        self.assertEqual(result[0], msg)
        self.assertEqual(result[1], 'crit')


class TestMaxScheduledWorkHours(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.scheduled_hours = 30
        self.job.max_work_hours = 30

    def test_pass_default(self):
        self.assertIsNone(
            rules.max_scheduled_work_hours(self.job)
            )

    def test_pass_argument(self):
        self.job.max_work_hours = 20
        self.assertIsNone(
            rules.max_scheduled_work_hours(self.job, maximum=40)
            )

    def test_fail_default(self):
        self.job.max_work_hours = 20
        result = rules.max_scheduled_work_hours(self.job)
        msg = 'Student cannot be assigned over {0} hours for this job.'.format(
            self.job.max_work_hours
            )
        self.assertEqual(result[0], msg)
        self.assertEqual(result[1], 'crit')


class TestTotalScheduledWorkHours(TestCase):
    def setUp(self):
        self.student = Student()
        self.add_jobs = [
            {'position_id': 'abc123',
            'job_profile': 'U500',
            'scheduled_hours': 10,
            'total_work_hours': 20,
            'start_date': dt.strptime('20180901', '%Y%m%d'),
            'end_date': dt.strptime('20181030', '%Y%m%d')
            },
            {'position_id': 'xyz234',
            'job_profile': 'U509',
            'scheduled_hours': 5,
            'total_work_hours': 30,
            'start_date': dt.strptime('20181001', '%Y%m%d'),
            'end_date': dt.strptime('20181130', '%Y%m%d')
            },
            ]
        self.job = Job()
        self.job.position_id = '123456'
        self.job.start_date = dt.strptime('20180901', '%Y%m%d')
        self.job.end_date = dt.strptime('20181215', '%Y%m%d')
        self.job.scheduled_hours = 5
        self.job.total_work_hours = 20
        self.job.ccyys_to_check = '20189'
        self.job.job_profile = 'U200'

    def test_success(self):
        self.student.additional_jobs = self.add_jobs
        self.assertIsNone(
            rules.total_scheduled_work_hours(self.job, self.student)
            )

    def test_success_change_job(self):
        self.add_jobs[0]['position_id'] = self.job.position_id
        self.add_jobs[0]['scheduled_hours'] = 20
        self.student.additional_jobs = self.add_jobs
        self.assertIsNone(
            rules.total_scheduled_work_hours(self.job, self.student)
            )

    def test_success_summer(self):
        self.add_jobs[0]['position_id'] = self.job.position_id
        self.add_jobs[0]['scheduled_hours'] = 20
        self.student.additional_jobs = self.add_jobs
        self.job.ccyys_to_check = '20186'
        self.assertIsNone(
            rules.total_scheduled_work_hours(self.job, self.student)
            )

    def test_success_grad_addl_job(self):
        self.add_jobs[0]['job_profile'] = 'G101'
        self.student.additional_jobs = self.add_jobs
        rules.BATCH = True
        self.assertIsNone(
            rules.total_scheduled_work_hours(self.job, self.student)
            )

    def test_grad_addl_job_online(self):
        self.add_jobs[0]['job_profile'] = 'G101'
        self.add_jobs[0]['scheduled_hours'] = 20
        self.student.additional_jobs = self.add_jobs
        self.job.total_work_hours = 40
        rules.BATCH = False
        self.assertIsInstance(
            rules.total_scheduled_work_hours(self.job, self.student), tuple
            )
        messages = rules.total_scheduled_work_hours(self.job, self.student)[0]
        self.assertIsInstance(messages, list)
        statuses = rules.total_scheduled_work_hours(self.job, self.student)[1]
        self.assertIsInstance(statuses, list)
        self.assertEqual(len(statuses), 1)
        self.assertEqual(len(statuses), len(messages))
        self.assertIn(self.add_jobs[0]['job_profile'], messages[0])
        self.assertEqual(statuses[0], 'non-crit')

    def test_too_many_hours_multiple(self):
        self.add_jobs[0]['scheduled_hours'] = 20
        self.student.additional_jobs = self.add_jobs
        self.assertIsInstance(
            rules.total_scheduled_work_hours(self.job, self.student), tuple
            )
        messages = rules.total_scheduled_work_hours(self.job, self.student)[0]
        self.assertIsInstance(messages, list)
        statuses = rules.total_scheduled_work_hours(self.job, self.student)[1]
        self.assertIsInstance(statuses, list)
        for status in statuses:
            self.assertEqual(status, 'crit')
        self.assertEqual(len(statuses), 2)
        self.assertEqual(len(statuses), len(messages))

    def test_too_many_hours_main_job(self):
        self.student.additional_jobs = self.add_jobs
        self.job.total_work_hours = 10
        self.assertIsInstance(
            rules.total_scheduled_work_hours(self.job, self.student), tuple
            )
        messages = rules.total_scheduled_work_hours(self.job, self.student)[0]
        self.assertIsInstance(messages, list)
        statuses = rules.total_scheduled_work_hours(self.job, self.student)[1]
        self.assertIsInstance(statuses, list)
        for status in statuses:
            self.assertEqual(status, 'crit')
        self.assertEqual(len(statuses), 1)
        self.assertEqual(len(statuses), len(messages))
        msg = 'Student cannot work over {0} hours for {1}.'.format(
            self.job.total_work_hours,
            self.job.job_profile,
            )
        self.assertEqual(messages[0], msg)

    def test_too_many_hours_addl_job(self):
        self.add_jobs[0]['total_work_hours'] = 5
        self.student.additional_jobs = self.add_jobs
        self.assertIsInstance(
            rules.total_scheduled_work_hours(self.job, self.student), tuple
            )
        messages = rules.total_scheduled_work_hours(self.job, self.student)[0]
        self.assertIsInstance(messages, list)
        statuses = rules.total_scheduled_work_hours(self.job, self.student)[1]
        self.assertIsInstance(statuses, list)
        for status in statuses:
            self.assertEqual(status, 'crit')
        self.assertEqual(len(statuses), 1)
        self.assertEqual(len(statuses), len(messages))
        msg = 'Student cannot work over {0} hours for {1}.'.format(
            self.add_jobs[0]['total_work_hours'],
            self.add_jobs[0]['job_profile'],
            )
        self.assertEqual(messages[0], msg)


class TestQuantityOfWork(TestCase):
    def setUp(self):
        self.student = Student()
        self.student.current_sem_hours = 10
        self.add_jobs = [
            {'position_id': 'abc123',
            'scheduled_hours': 10,
            'start_date': dt.strptime('20180901', '%Y%m%d'),
            'end_date': dt.strptime('20181030', '%Y%m%d')
            },
            {'position_id': 'xyz234',
            'scheduled_hours': 10,
            'start_date': dt.strptime('20181001', '%Y%m%d'),
            'end_date': dt.strptime('20181130', '%Y%m%d')
            },
            ]
        self.job = Job()
        self.job.position_id = '123456'
        self.job.start_date = dt.strptime('20180901', '%Y%m%d')
        self.job.end_date = dt.strptime('20181215', '%Y%m%d')
        self.job.scheduled_hours = 10

    def test_success(self):
        self.job.additional_jobs = self.add_jobs
        self.assertIsNone(rules.quantity_of_work(self.student, self.job))

    def test_too_many_work_hours(self):
        self.add_jobs[0]['scheduled_hours'] = 20
        self.job.additional_jobs = self.add_jobs
        self.assertIsInstance(
            rules.quantity_of_work(self.student, self.job), tuple
            )
        self.assertIsInstance(
            rules.quantity_of_work(self.student, self.job)[0], str
            )
        self.assertEqual(
            rules.quantity_of_work(self.student, self.job)[1], 'non-crit'
            )

    def test_success_change_job(self):
        self.add_jobs[0]['position_id'] = self.job.position_id
        self.add_jobs[0]['scheduled_hours'] = 20
        self.job.additional_jobs = self.add_jobs
        self.assertIsNone(rules.quantity_of_work(self.student, self.job))

    def test_too_many_credit_hours(self):
        self.student.current_sem_hours = 20
        self.job.additional_jobs = self.add_jobs
        self.assertIsInstance(
            rules.quantity_of_work(self.student, self.job), tuple
            )
        self.assertIsInstance(
            rules.quantity_of_work(self.student, self.job)[0], str
            )
        self.assertEqual(
            rules.quantity_of_work(self.student, self.job)[1], 'non-crit'
            )


class TestIsDegreeSeeking(TestCase):
    def test_degree_seeking(self):
        student = Student()
        student.degree_seeking = True
        self.assertIsNone(rules.is_degree_seeking(student))

    def test_not_degree_seeking(self):
        student = Student()
        student.degree_seeking = False
        self.assertIsInstance(rules.is_degree_seeking(student), tuple)
        self.assertIsInstance(rules.is_degree_seeking(student)[0], str)
        self.assertEqual(rules.is_degree_seeking(student)[1], 'crit')


class TestPostGraduation(TestCase):
    def setUp(self):
        self.original_current_sem = rules.CURRENT_SEM
        self.student = Student()
        self.student.hr_deadline = dt.now() + timedelta(days=1)
        self.student.current_sem_hours = 12
        self.student.prev_sem_hours = 12
        self.job = Job()
        self.job.job_family = 'Undergraduate Student Academic'
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')

    def tearDown(self):
        rules.CURRENT_SEM = self.original_current_sem

    def test_undergrad_non_summer_success(self):
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')
        self.student.is_grad_student = mock.Mock(return_value=False)
        result = rules.post_graduation(self.student, self.job)
        self.assertIsNone(result)

    def test_allow_grads(self):
        self.student.is_grad_student = mock.Mock(return_value=True)
        result = rules.post_graduation(
            self.student,
            self.job,
            allow_grad_students=True,
            )
        self.assertIsNone(result)

    def test_no_grads(self):
        self.student.is_grad_student = mock.Mock(return_value=True)
        result = rules.post_graduation(
            self.student,
            self.job,
            )
        self.assertIsInstance(result, tuple)
        self.assertIn('Grad students', result[0])
        self.assertEqual(result[1], 'crit')

    def test_spring_summer_undergrad_success(self):
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        rules.CURRENT_SEM = 2
        self.student.is_grad_student = mock.Mock(return_value=False)
        result = rules.post_graduation(self.student, self.job)
        self.assertIsNone(result)

    def test_summer_summer_undergrad_success(self):
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        rules.CURRENT_SEM = 6
        self.student.is_grad_student = mock.Mock(return_value=False)
        result = rules.post_graduation(self.student, self.job)
        self.assertIsNone(result)

    def test_undergrad_crit(self):
        rules.CURRENT_SEM = 6
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        self.student.is_grad_student = mock.Mock(return_value=False)
        self.student.hr_deadline = dt.now() - timedelta(days=1)
        self.student.prev_sem_hours = 3
        result = rules.post_graduation(
            self.student,
            self.job,
            )
        self.assertIsInstance(result, tuple)
        self.assertIn('must be enrolled', result[0])
        self.assertEqual(result[1], 'crit')

    def test_undergrad_noncrit(self):
        rules.CURRENT_SEM = 6
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        self.student.is_grad_student = mock.Mock(return_value=False)
        self.student.prev_sem_hours = 3
        result = rules.post_graduation(
            self.student,
            self.job,
            )
        self.assertIsInstance(result, tuple)
        self.assertIn('must be enrolled', result[0])
        self.assertEqual(result[1], 'non-crit')


class TestSAPStatus(TestCase):
    def setUp(self):
        self.student = Student()
        self.student.work_study = {
            'ok_to_disburse': True,
            'disbursement_msg': 'foobar',
            }
        self.student.hr_deadline = dt.now() + timedelta(days=1)

    def test_no_sap_status(self):
        self.assertIsNone(rules.sap_status(self.student))

    def test_sap_status_non_crit(self):
        self.student.work_study['ok_to_disburse'] = False
        self.assertIsInstance(rules.sap_status(self.student), tuple)
        self.assertEqual(
            rules.sap_status(self.student)[0],
            self.student.work_study['disbursement_msg'],
            )
        self.assertEqual(rules.sap_status(self.student)[1], 'non-crit')

    def test_sap_status_crit(self):
        self.student.work_study['ok_to_disburse'] = False
        self.student.hr_deadline = dt.now() - timedelta(days=1)
        self.assertIsInstance(rules.sap_status(self.student), tuple)
        self.assertEqual(
            rules.sap_status(self.student)[0],
            self.student.work_study['disbursement_msg'],
            )
        self.assertEqual(rules.sap_status(self.student)[1], 'crit')


class TestWSCheck(TestCase):
    def setUp(self):
        self.student = Student()
        self.student.work_study = {
            'ws_error_returned': False,
            'disbursement_msg': 'foobar',
            }

    def test_no_ws_error(self):
        self.assertIsNone(rules.ws_check(self.student))

    def test_ws_error(self):
        self.student.work_study['ws_error_returned'] = True
        self.assertIsInstance(rules.ws_check(self.student), tuple)
        self.assertEqual(
            rules.ws_check(self.student)[0],
            self.student.work_study['disbursement_msg'],
            )
        self.assertEqual(rules.ws_check(self.student)[1], 'crit')


class TestWSFTDatesValid(TestCase):
    def setUp(self):
        self.job = Job()
        self.job.start_date = dt.strptime('20190701', '%Y%m%d')
        self.job.end_date = dt.strptime('20190715', '%Y%m%d')

    def test_summer_dates(self):
        self.assertIsNone(rules.ws_ft_dates_valid(self.job))

    def test_spring_start(self):
        self.job.start_date = dt.strptime('20190201', '%Y%m%d')
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job), tuple)
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job)[0], str)
        self.assertEqual(rules.ws_ft_dates_valid(self.job)[1], 'crit')

    def test_fall_end(self):
        self.job.end_date = dt.strptime('20191001', '%Y%m%d')
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job), tuple)
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job)[0], str)
        self.assertEqual(rules.ws_ft_dates_valid(self.job)[1], 'crit')

    def test_non_summer(self):
        self.job.start_date = dt.strptime('20191001', '%Y%m%d')
        self.job.end_date = dt.strptime('20191031', '%Y%m%d')
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job), tuple)
        self.assertIsInstance(rules.ws_ft_dates_valid(self.job)[0], str)
        self.assertEqual(rules.ws_ft_dates_valid(self.job)[1], 'crit')

