from datetime import datetime as dt, time, timedelta
import logging

from .constants import (
    FALL_START,
    SPRING_START,
    SUMMER_START,
    SUMMER_2_START,
    )


def determine_semester(date):
    year = dt.strftime(date, '%Y')
    spring = dt.strptime(year+SPRING_START, '%Y%m/%d')
    fall = dt.strptime(year+FALL_START, '%Y%m/%d')
    summer = dt.strptime(year+SUMMER_START, '%Y%m/%d')

    if spring >= date >= fall - timedelta(days=365):
        sem = 9
    elif spring + timedelta(days=365) >= date >= fall:
        sem = 9
    elif summer >= date >= spring:
        sem = 2
    elif fall >= date >= summer:
        sem = 6

    return sem


def determine_summer_semester(start_date, ccyys_to_check):
    year = ccyys_to_check[:4]
    summer_2 = dt.strptime(year+SUMMER_2_START, '%Y%m/%d')

    if start_date < summer_2:
        return '6-1'
    else:
        return '6-2'


def determine_current_semester():
    year = dt.strftime(dt.now(), '%Y')

    sem = determine_semester(dt.now())

    ccyys = year + str(sem)

    return ccyys


def determine_ccys(date):
    year = int(dt.strftime(date, '%Y'))
    current_year = int(dt.strftime(dt.now(), '%Y'))
    sem = determine_semester(date)
    spring = dt.strptime(str(year)+SPRING_START, '%Y%m/%d')
    new_year = dt.strptime(str(year)+'1/1', '%Y%m/%d')

    if sem == 9:
        if year == current_year + 1:
            year = current_year
        elif new_year <= date < spring:
            year -= 1

    return str(year) + str(sem)


def jobs_overlap(addl_job, job):
    if (addl_job['start_date'] >= job.start_date
        and addl_job['start_date'] < job.end_date):
        return True
    elif (addl_job['start_date'] < job.start_date
        and addl_job['end_date'] > job.start_date):
        return True
    else:
        return False
