from datetime import datetime as dt, timedelta
from unittest import TestCase, mock

from .data import (
    FAIL_RESPONSE_XML,
    GOOD_RESPONSE_XML,
    Job,
    Response,
    SoapRequest,
    Student,
    TestSettings,
    Validator,
    )
from eligibility.constants import *
import eligibility.core as core


class DeterminsEligibility(TestCase):
    def setUp(self):
        self.original_UGNAValidator = core.UGNAValidator
        self.original_UGANTValidator = core.UGANTValidator
        self.original_WSNonAcaValidator = core.WSNonAcaValidator
        self.original_WSAcaValidator = core.WSAcaValidator
        self.student = Student()
        self.student.is_on_exception_table = mock.Mock(return_value=False)
        self.job = Job()
        self.job.job_profile = 'U101'

    def tearDown(self):
        core.UGNAValidator = self.original_UGNAValidator
        core.UGANTValidator = self.original_UGANTValidator
        core.WSNonAcaValidator = self.original_WSNonAcaValidator
        core.WSAcaValidator = self.original_WSAcaValidator

    def test_on_exception_table(self):
        self.student.is_on_exception_table = mock.Mock(return_value=True)
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], None)
        self.assertEqual(result['msg'], 'Student is on exception table.')

    def test_grad_student(self):
        self.job.job_profile = 'G101'
        self.student.grad_eligibility = 1
        self.student.grad_criticality = 'Critical'
        self.student.get_grad_messages = mock.Mock(return_value='foobar')
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], 'Critical')
        self.assertEqual(result['msg'], 'foobar')

    def test_non_aca(self):
        core.UGNAValidator = Validator
        self.job.job_family = 'Undergraduate Student Non-Academic'
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], 'Warning')
        self.assertEqual(result['msg'], 'Fakey. Fake. Messages.')

    def test_aca(self):
        core.UGANTValidator = Validator
        self.job.job_family = 'Undergraduate Student Academic'
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], 'Warning')
        self.assertEqual(result['msg'], 'Fakey. Fake. Messages.')

    def test_ws_non_aca(self):
        core.WSNonAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = WS_STATE_EX_CODE_NON_ACA
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], 'Warning')
        self.assertEqual(result['msg'], 'Fakey. Fake. Messages.')

    def test_ws_aca(self):
        core.WSAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = WS_STATE_EX_CODE_ACA
        result = core.determine_eligibility(self.job, self.student)
        self.assertEqual(result['eligible'], 1)
        self.assertEqual(result['warning_level'], 'Warning')
        self.assertEqual(result['msg'], 'Fakey. Fake. Messages.')

    def test_invalid_ws_classification(self):
        core.WSAcaValidator = Validator
        self.job.job_family = 'Work-Study'
        self.job.job_classification = 'foobar'
        self.assertRaises(
            core.InvalidJobProfileError,
            core.determine_eligibility,
            self.job, self.student,
            )

    def test_invalid_job_family(self):
        self.job.job_family = 'foobar'
        self.assertRaises(
            core.InvalidJobProfileError,
            core.determine_eligibility,
            self.job, self.student,
            )


class RespondToWorkday(TestCase):
    def setUp(self):
        self.original_SoapRequest = core.SoapRequest
        self.original_settings = core.settings
        core.SoapRequest = SoapRequest
        core.settings = TestSettings
        self.student = Student()
        self.job = Job()
        self.job.job_profile = 'U101'
        self.eligibility = {
            'eligible': 0,
            'warning_level': 'Warning',
            'msg': 'Not eligibile.',
            }
        self.event = 'ABC123'

    def tearDown(self):
        core.SoapRequest = self.original_SoapRequest
        core.settings = self.original_settings

    def test_success(self):
        core.SoapRequest.send_request = mock.Mock(
            return_value=Response(GOOD_RESPONSE_XML)
            )
        self.assertIsNone(
            core.respond_to_workday(self.eligibility, self.event)
            )

    def test_failure(self):
        core.SoapRequest.send_request = mock.Mock(
            return_value=Response(FAIL_RESPONSE_XML)
            )
        self.assertIsNone(
            core.respond_to_workday(self.eligibility, self.event)
            )
